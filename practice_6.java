import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;

public class practice_6 {
    public static void main(String[] args) {
        ArrayList<Integer> randomNumbers = new ArrayList<Integer>(5);
        for (int i = 0; i < 25; i++) {
            randomNumbers.add((int) (Math.random() * 10));
        }

        HashMap<Integer, Integer> Unic = new HashMap<>();
        Set<Integer> UnicSet = new HashSet<>();
        for (int i = 0; i < randomNumbers.size(); i++) {
            UnicSet.add(randomNumbers.get(i));
        }

        for (int value : UnicSet) {
            Unic.put(randomNumbers.indexOf(value), value);
        }


        System.out.println(randomNumbers);
        System.out.println(UnicSet);
        System.out.println(Unic);
    }
}